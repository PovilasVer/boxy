/**
 * @author	Povilas Versockas p.versockas@gmail.com
 * @licence	This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License. 
 *			To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/.
 */
/**TODO:

r for retry+
p for pause+
don't let player set this in controls+
Document it :)

TEST TEST TEST IE/Firefox
Animation
2 player levels

assetLoader!!!
gGameEngine.stop()?

localStorage change to cookies

**/
/**
Changes from posting:
fixed bug where it didn't work any other browsers
Menu class
Setting control bug
made key r restart the level
added level modes (currently only works "standart" and "2Alive", but in future timed challanges and other stuff
added 2Player levels
Finish bug
**/
/*
	document.getElementById('canvasContainer').width = x;
	document.getElementById('canvasContainer').height = y;
	document.getElementById('canvas').width = x;
	document.getElementById('canvas').height = y;
	
var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
*/



function play( mode, level ){
	gGameEngine.pause();
	init();
	gLevelLoader.load(mode, level);
	
	gGameEngine.resume();
	
}

function playNext(){
	gUI.toggleGameMenu('win');
	
	var mode = gLevelLoader.currentLevel.mode;
	play(mode, gLevelLoader.currentLevel.level*1+1);
}

function playCurrent(){
	if(gUI.active=='lose')
		gUI.toggleGameMenu('lose');
	play(gLevelLoader.currentLevel.mode, gLevelLoader.currentLevel.level);

}

function resume(){
	gUI.toggleGameMenu('pause');
	gGameEngine.resume();
}

function init(){
	gUI.menu('main');
	active='game';
	gUI.showCanvas();
	
	if(gGameEngine.initialized)
		return ;
	gGameEngine.setup();
	gInputEngine.setup();

	requestAnimFrame(gGameEngine.update);
}

function quit(){
	gUI.clearGameMenus();
	if(gUI.active == 'pause')
		gGameEngine.stop();
	gUI.menu('main');
	gUI.showUI();
}

function changeControls(player, action){
	gUI.toggleNotification();
	window.addEventListener('keydown', function(event){
		var pressed = event.keyCode; 
		for(var i in gSaveMananger.controlsPlayer1){
			if(gSaveMananger.controlsPlayer1[i] == pressed ){
				gUI.toggleNotification();
				this.removeEventListener('keydown',arguments.callee,false);
				return ;		
			}
		}
		for(var i in gSaveMananger.controlsPlayer2){
			if(gSaveMananger.controlsPlayer2[i] == pressed ){
				gUI.toggleNotification();
				this.removeEventListener('keydown',arguments.callee,false);
				return ;
		
			}
		}
		//cant unbind pause or retry
		if(pressed == gSaveMananger.retry || pressed == gSaveMananger.save ){
			gUI.toggleNotification();
			this.removeEventListener('keydown',arguments.callee,false);
			return ;
		}
		gUI.toggleNotification();
		document.getElementById(player+''+action).innerHTML=gInputEngine.KEY.getKeyByValue(pressed);
		
		gSaveMananger.saveControls(player,action,pressed);
		
		this.removeEventListener('keydown',arguments.callee,false);
	});
}

function toggleMute(){
	gSM.togglemute();
	gUI.toggleMute();
}

/* Page Visibility API
http://www.html5rocks.com/en/tutorials/pagevisibility/intro/
*/
function getHiddenProp(){
	var prefixes = ['webkit','moz','ms','o'];
	
	// if 'hidden' is natively supported just return it
	if ('hidden' in document) return 'hidden';
	
	// otherwise loop over all the known prefixes until we find one
	for (var i = 0; i < prefixes.length; i++){
		if ((prefixes[i] + 'Hidden') in document) 
			return prefixes[i] + 'Hidden';
	}

	// otherwise it's not supported
	return null;
}

var visProp = getHiddenProp();
if (visProp) {
  var evtname = visProp.replace(/[H|h]idden/,'') + 'visibilitychange';
  document.addEventListener(evtname, function(){
	if(gUI.active!='game')
		return ;
	gUI.toggleGameMenu('pause');
	gGameEngine.pause();	
  });
}