/*Copyright 2012 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
#limitations under the License.*/

//This was taken from Udacity code, so I left Grits game license
//Also a lot of my modifications, most of them are in the update method

GameEngineClass = Class.extend({
	_pause: false,
	_stop: false,
	initialized: false,
	players: [],
    entities: [],
    factory: {},
    _deferredKill: [],

    //-----------------------------
    init: function () {},

    //-----------------------------
    setup: function () {
		if(gGameEngine.initialized)
			return ;
			
        // Create physics engine
        gPhysicsEngine.create();
		
        // Add contact listener
        gPhysicsEngine.addContactListener({

            PostSolve: function (bodyA, bodyB) {
				 var uA = bodyA ? bodyA.GetUserData() : null;
                var uB = bodyB ? bodyB.GetUserData() : null;
				
                if (uA !== null) {
                    if (uA.ent !== null && uA.ent.onTouch) {			   
                        uA.ent.onTouch(bodyB, null);
                    }
                }

                if (uB !== null) {
                    if (uB.ent !== null && uB.ent.onTouch) {
                        uB.ent.onTouch(bodyA, null);
                    }
                }
            }
        });
			
		gGameEngine.inicialized = true;
    },

    spawnEntity: function (typename) {
        var ent = new (gGameEngine.factory[typename])();
		if(typename=='Player'){
			gGameEngine.players.push(ent);
		}
			gGameEngine.entities.push(ent);
		
        return ent;
    },

    update: function () {
		
		if(gGameEngine._stop){
			gGameEngine._stop = false;
			gGameEngine._pause = false;
			return ;
		}
		if(gInputEngine.pressed('pause')){
			gUI.toggleGameMenu('pause');
			if(gGameEngine._pause){
				gGameEngine.resume();
			}else{
				gGameEngine.pause();
			}
			gInputEngine.clearAllState();
		}
		if(gInputEngine.pressed('retry')){
			gGameEngine.players = [];
			gGameEngine.entities = [];
			gGameEngine._stop = false;
			gGameEngine._pause = false;
			gGameEngine.initialized = false;
			gInputEngine.clearAllState();
			playCurrent();
			
			return ;
		}
		
		if(gGameEngine._pause || !gLevelLoader.loaded ){
			
			requestAnimFrame(gGameEngine.update);
			return ;
		}
		
		for (var i = 0; i < gGameEngine.entities.length; i++) {
            var ent = gGameEngine.entities[i];
            if(!ent._killed) {
                ent.update();
            } else {
				gPhysicsEngine.removeBody(ent.physBody);
				ent.physBody = null;
				gGameEngine._deferredKill.push(ent);
            }
        }

        for (var j = 0; j < gGameEngine._deferredKill.length; j++) {	
			if(gGameEngine._deferredKill[j].type=="Player")
				gGameEngine.players.erase(gGameEngine._deferredKill[j]);
       
            gGameEngine.entities.erase(gGameEngine._deferredKill[j]);
        }
		
        gGameEngine._deferredKill = [];

		//player lost
		if( (gLevelLoader.mode=="standart" && gGameEngine.players.length == 0) || (gLevelLoader.mode=="2Alive" && gGameEngine.players.length <= 1)  ){
			gGameEngine.stop();
			gUI.toggleGameMenu('lose');
			
			//listen for retry key
			window.addEventListener('keydown', function(event){
				if(event.keyCode == gSaveMananger.retry){
					gGameEngine.players = [];
					gGameEngine.entities = [];
					gGameEngine._stop = false;
					gGameEngine._pause = false;
					gGameEngine.initialized = false;
					playCurrent();
					this.removeEventListener('keydown',arguments.callee,false);
					
					return ;
				}	
			});
		
		// player won
		}else if(gGameEngine.entities.length == gGameEngine.players.length){
			gGameEngine.stop();
			var mode = gLevelLoader.currentLevel.mode;
			if(gSaveMananger.getLevel(mode) == gLevelLoader.currentLevel.level ){
				gSaveMananger.incrementLevel( mode );
				gUI.updateLevelMenu( mode, gSaveMananger.getLevel(mode) );
			}
			if( (gLevelLoader.currentLevel.mode=="1Player" && gLevelLoader.currentLevel.level == ( total1PlayerLevels - 1 ) ) ||
			gLevelLoader.currentLevel.mode=="2Players" && gLevelLoader.currentLevel.level == ( total2PlayersLevels - 1 )){
				gUI.toggleGameMenu('finish');
			}else{
				gUI.toggleGameMenu('win');
			}
		}
        // Update physics engine
        gPhysicsEngine.update();
		
		//keyboard input
		for (var i = 0; i < gGameEngine.players.length; ++i) {
			var player = gGameEngine.players[i];
			if(player._killed)
				continue ;
			var id = player.id;
			

			if(gInputEngine.actions['move-up'+id]){
				player.moveY(-1);
			}
			else if (gInputEngine.actions['move-down'+id]){
				player.moveY(1);
			}else{
				player.stopX();
			}
			
			if(gInputEngine.actions['move-left'+id]){
				player.moveX(-1);
			}
			else if (gInputEngine.actions['move-right'+id]){
				player.moveX(1);
			}else{
				player.stopY();
			}
		}

		gInputEngine.clearPressed();
		stats.update();

		requestAnimFrame(gGameEngine.update);
    },
	pause: function(){
		gGameEngine._pause = true;
	},
	resume: function(){
		gGameEngine._pause = false;
	},
	stop: function(){
		gGameEngine._stop = true;
		/*TODO: remove entities, players, unbindAll,  clearContact listener */
		gGameEngine.initialized = false;
		gGameEngine.players = [];
		gGameEngine.entities = [];
	}

});

gGameEngine = new GameEngineClass();
