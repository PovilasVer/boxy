/**
 * @author	Povilas Versockas p.versockas@gmail.com
 * @licence	This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License. 
 *			To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/.
 */

//Some of menu functionality
MenuClass = Class.extend({
	active: "main",
	gameMenus: ["win","pause","finish","lose"], 
	notification: false,
	mute: true,
	init: function (){
	
	
		/* loads 1 player menu levels */
		var element1 = document.getElementById('1PlayerLevels');
		for ( var level = 0; level < total1PlayerLevels; ++level){
			var a = document.createElement("button");
			a.innerHTML = level;
			if( level > gSaveMananger.level1Player*1 ){
				a.setAttribute('class', 'forbidden');
			}else{
				a.onclick = function(){ playSoundInstance('sounds/menu_select.wav'); play("1Player", this.innerHTML) };
				a.onmouseover = function(){ playSoundInstance('sounds/menu_bump.wav'); };

			}
			
			element1.appendChild(a);
		}
		
		/* loads 2 players menu levels */
		var element2 = document.getElementById('2PlayersLevels');
		for ( var level = 0; level < total2PlayersLevels; ++level){
			var a = document.createElement("button");
			
			a.innerHTML = level;
			if( a.innerHTML > gSaveMananger.level2Players*1 ){
				a.setAttribute('class', 'forbidden');
			}else{
				a.onclick = function(){ playSoundInstance('sounds/menu_select.wav');  play("2Players", this.innerHTML) };
				a.onmouseover = function(){ playSoundInstance('sounds/menu_bump.wav'); };
			}
			
			element2.appendChild(a);
		}
		
		
		/* loads current controls to control menu */
		document.getElementById('Player1move-up').innerHTML=gInputEngine.KEY.getKeyByValue(gSaveMananger.controlsPlayer1['move-up']);
		document.getElementById('Player1move-down').innerHTML=gInputEngine.KEY.getKeyByValue(gSaveMananger.controlsPlayer1['move-down']);
		document.getElementById('Player1move-left').innerHTML=gInputEngine.KEY.getKeyByValue(gSaveMananger.controlsPlayer1['move-left']);
		document.getElementById('Player1move-right').innerHTML=gInputEngine.KEY.getKeyByValue(gSaveMananger.controlsPlayer1['move-right']);
		document.getElementById('Player2move-up').innerHTML=gInputEngine.KEY.getKeyByValue(gSaveMananger.controlsPlayer2['move-up']);
		document.getElementById('Player2move-down').innerHTML=gInputEngine.KEY.getKeyByValue(gSaveMananger.controlsPlayer2['move-down']);
		document.getElementById('Player2move-left').innerHTML=gInputEngine.KEY.getKeyByValue(gSaveMananger.controlsPlayer2['move-left']);
		document.getElementById('Player2move-right').innerHTML=gInputEngine.KEY.getKeyByValue(gSaveMananger.controlsPlayer2['move-right']);

	},
	menu: function( name ){
		if(gUI.active != 'game')
			document.getElementById(gUI.active+'Menu').style.display = "none";
		
			document.getElementById(name+'Menu').style.display = "block";
			gUI.active = name;
	},
	toggleGameMenu: function( name ){
		if(gUI.active == name){
			document.getElementById(name+'Menu').style.display = "none";
			gUI.active = 'game';
		}else{
			document.getElementById(name+'Menu').style.display = "block";	
			gUI.active = name;
		}
	},
	clearGameMenus: function(){
		for (var i=0 ;i < gUI.gameMenus.length; ++i){
			document.getElementById(gUI.gameMenus[i]+'Menu').style.display = "none";
		}
	},
	showCanvas: function(){
		document.getElementById('UI').style.display='none';
		document.getElementById('canvasContainer').style.display='block';
		gUI.active = 'game';
	},
	showUI: function(){
		gUI.active = 'main';
		document.getElementById('UI').style.display='block';
		document.getElementById('canvasContainer').style.display='none';
	},
	toggleNotification: function(){
		if(gUI.notification){
			document.getElementById('notification').style.display="none";
			gUI.notification = false;
		}else{
			document.getElementById('notification').style.display="block";
			gUI.notification = true;
		}
	},
	toggleMute: function(){
		if(gUI.mute){
			document.getElementById('mute').innerHTML='Unmute sound';
			gUI.mute = false;
		}else{
			document.getElementById('mute').innerHTML='Mute sound';
			gUI.mute = true;
		}
	},
	updateLevelMenu: function(mode, level ){
		var elements = document.getElementById(mode+"Levels").childNodes;
		for (var i = 0; i < elements.length; ++i) {
			 if (elements[i].innerHTML.indexOf(level) !== -1) {
				 elements[i].className="";
				 elements[i].onclick = function(){ playSoundInstance('sounds/menu_select.wav');  play(mode, elements[i].innerHTML) };
				 elements[i].onmouseover = function(){ playSoundInstance('sounds/menu_bump.wav'); };
				 break;
			 }
		}
	}
	
});
gUI = new MenuClass();

