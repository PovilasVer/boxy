/*Copyright 2011 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
#limitations under the License.*/

//This was taken from Udacity code, so I left Grits game license
//Also a lot of my modifications, most of them are in the update method

// These are global shorthands we declare for Box2D primitives
// we'll be using very frequently.
Vec2 = Box2D.Common.Math.b2Vec2;
BodyDef = Box2D.Dynamics.b2BodyDef;
Body = Box2D.Dynamics.b2Body;
FixtureDef = Box2D.Dynamics.b2FixtureDef;
Fixture = Box2D.Dynamics.b2Fixture;
World = Box2D.Dynamics.b2World;
MassData = Box2D.Collision.Shapes.b2MassData;
PolygonShape = Box2D.Collision.Shapes.b2PolygonShape;
CircleShape = Box2D.Collision.Shapes.b2CircleShape;
DebugDraw = Box2D.Dynamics.b2DebugDraw;
RevoluteJointDef = Box2D.Dynamics.Joints.b2RevoluteJointDef;
PhysicsEngineClass = Class.extend({
    world: null,

    PHYSICS_LOOP_HZ : 1.0 / 60.0,

    //-----------------------------------------
    create: function () {
        gPhysicsEngine.world = new World(
            new Vec2(0, 0), // Gravity vector
            false           // Don't allow sleep
        );
		var debugDraw = new DebugDraw();
		debugDraw.SetSprite(document.getElementById("canvas").getContext("2d"));
		debugDraw.SetDrawScale(30);
		debugDraw.SetFillAlpha(0);
		debugDraw.SetLineThickness(0);
		debugDraw.SetFlags(DebugDraw.e_shapeBit | DebugDraw.e_jointBit);
		gPhysicsEngine.world.SetDebugDraw(debugDraw);
    },

    //-----------------------------------------
    update: function () {
        var start = Date.now();
	   gPhysicsEngine.world.Step(
            gPhysicsEngine.PHYSICS_LOOP_HZ,    //frame-rate
            10,                 //velocity iterations
            10                  //position iterations
        );
        gPhysicsEngine.world.ClearForces();
		gPhysicsEngine.world.DrawDebugData();
		
		//draws are also handeled by box2d :)
	    for(var b = gPhysicsEngine.world.m_bodyList; b != null; b = b.m_next){
            if(b.GetUserData()){
                size = b.GetUserData().size;
				ctx.save();
                ctx.translate(b.GetPosition().x*30,b.GetPosition().y*30);
                ctx.rotate(b.GetAngle());
                ctx.fillStyle=b.GetUserData().color;
				ctx.fillRect(-size/2-0.5,-size/2-0.5,size+1.5,size+1.5);
				//ctx.drawImage(b.GetUserData(),-b.GetUserData().width/2,-b.GetUserData().height/2);
                ctx.restore();
            }
        }

		return(Date.now() - start);
    },

    //-----------------------------------------
    addContactListener: function (callbacks) {
        var listener = new Box2D.Dynamics.b2ContactListener();
		for ( var i in callbacks){
		  if(callbacks[i]) listener[i] = function (contact/*, impulse*/) {
           // console.log(impulse);
			callbacks[i](contact.GetFixtureA().GetBody(),
                                contact.GetFixtureB().GetBody()); /*,
                                impulse.normalImpulses[0]);*/
			};
		}
		/*
      */

        gPhysicsEngine.world.SetContactListener(listener);
    },

    //-----------------------------------------
    registerBody: function (bodyDef) {
        var body = gPhysicsEngine.world.CreateBody(bodyDef);
        return body;
    },

    //-----------------------------------------
    addBody: function (entityDef) {
        var bodyDef = new BodyDef();

        var id = entityDef.id;

        if(entityDef.type == 'static') {
            bodyDef.type = Body.b2_staticBody;
        } else {
            bodyDef.type = Body.b2_dynamicBody;
        }

        bodyDef.position.x = entityDef.x;
        bodyDef.position.y = entityDef.y;

        if(entityDef.userData)  bodyDef.userData = entityDef.userData;

        var body = this.registerBody(bodyDef);
        var fixtureDefinition = new FixtureDef();
		fixtureDefinition.restitution = 0.1;
        if(entityDef.useBouncyFixture) {
            fixtureDefinition.density = 1.0;
            fixtureDefinition.friction = 0;
            fixtureDefinition.restitution = 1.0;
        }

        // Now we define the shape of this object as a box
        fixtureDefinition.shape = new PolygonShape();
        fixtureDefinition.shape.SetAsBox(entityDef.halfWidth, entityDef.halfHeight);
        body.CreateFixture(fixtureDefinition);
		
        return body;
    },

    //-----------------------------------------
    removeBody: function (obj) {
        gPhysicsEngine.world.DestroyBody(obj);
    }

});

var gPhysicsEngine = new PhysicsEngineClass();

