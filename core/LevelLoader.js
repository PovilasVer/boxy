/**
 * @author	Povilas Versockas p.versockas@gmail.com
 * @licence	This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License. 
 *			To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/.
 */

function xhrGet(reqUri,callback) {
	var xhr = new XMLHttpRequest();

	xhr.open("GET", reqUri, true);
	xhr.onload = callback;

	xhr.send();
}

var total1PlayerLevels = 12;
var total2PlayersLevels = 6;

LevelLoaderClass = Class.extend({
	currentLevel: null,
	loaded: false,
	init: function () {},
	setup: function() {},
	mode: "standart",
	
	load: function(mode, level) {
		gLevelLoader.loaded = false;
		
		if(level > window['total'+mode+'Levels']-1)
			return 0;
		
		xhrGet("levels/"+mode+"/"+level+".json", function (data) {
			gLevelLoader.parseLevelJSON(this.responseText);
		});
		gLevelLoader.currentLevel  = {"mode": mode, "level":  level };
		
		return true;
	},
	parseLevelJSON: function(data){
		var parsed = JSON.parse(data);
		if(parsed.mode){
			this.mode = parsed.mode;
		}else{
			this.mode = "standart";
		}
		
		for (var i = 0; i < parsed.entities.length; ++i){
			var en = parsed.entities[i];
			
			var newEnt = gGameEngine.spawnEntity(en.type);
			newEnt.init.apply(newEnt, en.init);
			if(en.methods){
				for (var y = 0; y < en.methods.length; ++y){
					var method = en.methods[y];
					newEnt[method.name].apply(newEnt, method.init);
				}			
			}
			
		}
		gLevelLoader.loaded = true;
		
	}

});
gLevelLoader = new LevelLoaderClass();