/**
 * @author	Povilas Versockas p.versockas@gmail.com
 * @licence	This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License. 
 *			To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/.
 */
//TODO : fallback to use cookies in case no localStorage

SaveManangerClass = Class.extend({
	//current 1 player mode level
	level1Player: 0,
	//current 2 players mode level
	level2Players: 0,
	controlsPlayer1: {"move-up": 38, "move-down": 40, "move-left": 37, "move-right": 39},
	controlsPlayer2: {"move-up": 87, "move-down": 83, "move-left": 65, "move-right": 68},
	pause: 80,
	retry: 82,
	init: function(){
		if (localStorage.level1Player){
			this.level1Player = localStorage.level1Player;
		}
		
		if(localStorage.level2Players){
			this.level2Players = localStorage.level2Players;
		}
		
		if(localStorage.controlsPlayer1){
			this.controlsPlayer1 = JSON.parse(localStorage.controlsPlayer1);
		}else{
			localStorage.controlsPlayer1=JSON.stringify(this.controlsPlayer1);
		}
		
		if(localStorage.controlsPlayer2){
			this.controlsPlayer2 = JSON.parse(localStorage.controlsPlayer2);
		}else{
			localStorage.controlsPlayer2=JSON.stringify(this.controlsPlayer2);
		}
		gInputEngine.bind(this.controlsPlayer1['move-up'], 'move-upPlayer1');
		gInputEngine.bind(this.controlsPlayer1['move-left'], 'move-leftPlayer1');
		gInputEngine.bind(this.controlsPlayer1['move-right'], 'move-rightPlayer1');
		gInputEngine.bind(this.controlsPlayer1['move-down'], 'move-downPlayer1');
		gInputEngine.bind(this.controlsPlayer2['move-up'], 'move-upPlayer2');
		gInputEngine.bind(this.controlsPlayer2['move-left'], 'move-leftPlayer2');
		gInputEngine.bind(this.controlsPlayer2['move-right'], 'move-rightPlayer2');
		gInputEngine.bind(this.controlsPlayer2['move-down'], 'move-downPlayer2');
		gInputEngine.bind(this.pause, 'pause');
		gInputEngine.bind(this.retry, 'retry');
	},
	getLevel: function(mode){
		return gSaveMananger['level'+mode];
	},
	saveLevel: function(mode, level){
		localStorage['level'+mode]=level;
		gSaveMananger['level'+mode]=level;
	},
	incrementLevel: function(mode){
		gSaveMananger['level'+mode] = gSaveMananger['level'+mode]*1 + 1;
		localStorage['level'+mode] = gSaveMananger['level'+mode];
	},
	saveControls: function(player, action, key){
		
		gInputEngine.unbind(gSaveMananger['controls'+player][action]);
		
		var parsed = JSON.parse(localStorage['controls'+player]);
		parsed[action] = key;
		localStorage['controls'+player] = JSON.stringify(parsed);
		
		gSaveMananger['controls'+player][action] = key;
	
		gInputEngine.bind(key, action+player);
	}
});
gSaveMananger = new SaveManangerClass();