/**
 * @author	Povilas Versockas p.versockas@gmail.com
 * @licence	This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License. 
 *			To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/.
 */
PlayerClass = EntityClass.extend({
    physBody: null,
	type: "Player",
    _killed: false,
	accelaration: 1,
	stopAccelaration: 0.3,
	velocityLimit: 5,
	id: null,
    init: function (x, y, size, color, settings) {
		if(x==null)
			return;
        this.parent(x, y, settings);
        this.id = settings.id;

		
		// Create our physics body;
        var entityDef = {
            id: "Player",
            x: x,
            y: y,
            halfHeight: size * 0.5 / 30,
            halfWidth: size * 0.5 / 30,
            damping: 0,
            angle: 0,
        /*    categories: ['projectile'],
            collidesWith: ['player'],*/
            userData: {
                "id": "Player",
                "ent": this,
				"size": size,
				"color": color,
				"score": 0
            }
        };

        this.physBody = gPhysicsEngine.addBody(entityDef);
        this.physBody.SetLinearVelocity(new Vec2(0, 0));
	
    },
    update: function () {
		if( this.physBody.GetPosition().x > 20.5 ){
			this.physBody.SetPosition( new Vec2( 0, this.physBody.GetPosition().y ) );
			
		}else if( this.physBody.GetPosition().x < 0 ){
			this.physBody.SetPosition( new Vec2( 20.5, this.physBody.GetPosition().y ) );
		}
		
		if( this.physBody.GetPosition().y > 13.6 ){
			this.physBody.SetPosition( new Vec2( this.physBody.GetPosition().x , 0 ) );
		}else if( this.physBody.GetPosition().y < 0 ){
			this.physBody.SetPosition( new Vec2( this.physBody.GetPosition().x , 13.6 ) );
		}
	},
	grow: function () {
		this.physBody.GetUserData().size += 2;
		this.physBody.GetFixtureList().m_shape.SetAsBox( this.physBody.GetUserData().size* 0.5 / 30, this.physBody.GetUserData().size* 0.5 / 30);
		this.physBody.GetUserData().score += 1;
		
		//increase speedLimit?
		//
	},
	increaseAccelaration: function (acc){
		this.accelaration += acc;
		this.velocityLimit += acc;
		this.stopAccelaration += acc/100;
	},
	//-----------------------------------------
    kill: function () {
		playSoundInstance('sounds/die.wav');
      
        this._killed = true;
    },
	
    //-----------------------------------------
    onTouch: function (otherBody, point, impulse) {
		
        if(!this.physBody) return false;
        if(!otherBody.GetUserData()) return false;

        var physOwner = otherBody.GetUserData();
	
        if(physOwner !== null) {
           
			if(physOwner.id=='Box'){
				if(physOwner.size > this.physBody.GetUserData().size){
					this.kill();
				}else{
					this.grow();
				}
			}
        }
        return true;
    },
	//takes either -1 or 1
	moveX: function( x ){
		var velo = this.physBody.GetLinearVelocity();
		if( Math.abs( velo.x + this.accelaration * x ) < this.velocityLimit){
			velo.x += this.accelaration * x;	
		}
	},
	moveY: function( y ){
		var velo = this.physBody.GetLinearVelocity();
		if( Math.abs( velo.y + this.accelaration * y ) < this.velocityLimit){
			velo.y += this.accelaration * y;
		}
	},
	stopX: function( ){
		var velo = this.physBody.GetLinearVelocity();
		
		if (velo.x < 0) {
			velo.x += this.stopAccelaration;
			if (velo.x >= 0) {
				velo.x = 0;
			}
		} else {
			velo.x -= this.stopAccelaration;
			if (velo.x <= 0) {
				velo.x = 0;
			}
		}
	},
	stopY: function() {
		var velo = this.physBody.GetLinearVelocity();
		
		if (velo.y < 0) {
			velo.y += this.stopAccelaration;
			if (velo.y >= 0) {
				velo.y = 0;
			}
		}else {
			velo.y -= this.stopAccelaration;
			if (velo.y <= 0) {
				velo.y = 0;
			}
		}
	}
	
});

gGameEngine.factory['Player'] = PlayerClass;

