/**
 * @author	Povilas Versockas p.versockas@gmail.com
 * @licence	This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License. 
 *			To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/.
 */
BoxClass = EntityClass.extend({
    physBody: null,
    _killed: false,
	
	//method data
	_keepSpeed: 0,
	_rotate: 0,
	_sleep: 0,
	_oldPos: null,
	_oldVelo: null,
	_follow: null,
	_killPlayer: null,
	
	timer : 0,
	
	//buffs
	_speedBuffer: 0,
    init: function (x, y, size, color) {
		if(x==null)
			return;
        this.parent(x, y);
		// Create our physics body;
        var entityDef = {
            id: "Box",
            x: x,
            y: y,
            halfHeight: size * 0.5 / 30,
            halfWidth: size * 0.5 / 30,
            damping: 0,
            angle: 0,
        /*    categories: ['projectile'],
            collidesWith: ['player'],*/
            userData: {
                "id": "Box",
                "ent": this,
				"size": size,
				"color": color,
				"startPos": {"x": x, "y": y}
            }
        };

        this.physBody = gPhysicsEngine.addBody(entityDef);
        this.physBody.SetLinearVelocity(new Vec2(0, 0));
    },
	
	update: function(){
		//special methods
		++this.timer;
		if ( this._sleep > this.timer ){
			return ;
		}
		
		if( this._sleep ){
			this.physBody.SetPosition(this._oldPos);
			this.physBody.SetLinearVelocity(this._oldVelo);
			this._sleep = 0;
		}
		
		if( this._follow!=null ){
			var myLength = this.physBody.GetLinearVelocity().Length();
			var playerVec = new Vec2(gGameEngine.players[this._follow].physBody.GetPosition().x, gGameEngine.players[this._follow].physBody.GetPosition().y); 
			var myVec = this.physBody.GetPosition();
			playerVec.Subtract(myVec)
			playerVec.Normalize();
			playerVec.Multiply(myLength);
			this.physBody.SetLinearVelocity(playerVec);
		}
		if( this._keepSpeed ){
			var vec = this.physBody.GetLinearVelocity();
			if(this._keepSpeed > vec.Length()){
				vec.Normalize();
				vec.Multiply(this._keepSpeed,this._keepSpeed);	
				//console.log('Multiplied ', this.physBody.GetLinearVelocity().Length(), 'vs', this._keepSpeed);
			
			}
		}

		if( this._rotate ){
			this.physBody.SetAngle( this.physBody.GetAngle() + this._rotate * 0.0175 );
		}
		
		//walls
		if( this.physBody.GetPosition().x > 20.5 ){
			this.physBody.SetPosition( new Vec2( 0, this.physBody.GetPosition().y ) );
			
		}else if( this.physBody.GetPosition().x < 0 ){
			this.physBody.SetPosition( new Vec2( 20.5, this.physBody.GetPosition().y ) );
		}
		
		if( this.physBody.GetPosition().y > 13.6 ){
			this.physBody.SetPosition( new Vec2( this.physBody.GetPosition().x , 0 ) );
		}else if( this.physBody.GetPosition().y < 0 ){
			this.physBody.SetPosition( new Vec2( this.physBody.GetPosition().x , 13.6 ) );
		}
	},
	move: function (x, y) {
		this.physBody.SetLinearVelocity(new Vec2(x, y));
	},
	sleep: function( time ){
		this._sleep = time;
		this._oldPos = new Vec2(this.physBody.GetPosition().x, this.physBody.GetPosition().y);
		var vec2 =  new Vec2(this.physBody.GetLinearVelocity().x, this.physBody.GetLinearVelocity().y);
		this._oldVelo = vec2;
		this.physBody.SetLinearVelocity( new Vec2(0,0) );
		this.physBody.SetPosition(new Vec2(-100,-100));
	},
	keepSpeed: function (speed){
		this._keepSpeed = speed;
	},
	
	buffSpeed: function(speed){
		this._speedBuffer = speed;
	},
	setRotation: function ( speed ){
		this._rotate = speed;
	},
	followPlayer: function ( id ){
		this._follow = id;
	},
	killPlayer: function ( playerId ){
		this._killPlayer = playerId;
	},
    //-----------------------------------------
    kill: function () {
		playSoundInstance('sounds/box_eat.wav');
        this._killed = true;
    },
	

    //-----------------------------------------
    onTouch: function (otherBody, point, impulse) {
        if(!this.physBody) return false;
        if(!otherBody.GetUserData()) return false;

        var physOwner = otherBody.GetUserData();
        
		//console.log(physOwner);
        if(physOwner !== null) {
            if(physOwner._killed) return false;

			if(physOwner.id=='Player'){
				if(physOwner.size > this.physBody.GetUserData().size){
				
					if(this._speedBuffer)
						physOwner.ent.increaseAccelaration(this._speedBuffer);
						if(physOwner.ent.id == this._killPlayer){
							physOwner.ent.kill();
						}
					this.kill();
				}
			}
        }

        return true;
    }

});

gGameEngine.factory['Box'] = BoxClass;

