#Boxy

![Boxy](http://s9.postimg.org/ei7tlbvxb/boxy.png)

This is HTML5 game made by me for the HTML5 game contest 

##Controls
**Arrow keys** to move,  **P** to pause level, **R** to restart level

##The game
Boxy is a puzzle/action game. You control the Green box and try to eat all the other boxes, but you can only eat smaller boxes than you. The game has 12 single player levels, enemy boxes have some fun skills like: following you, make you move faster, or appear from nowhere :) The most awesome part of this game is that game world is dynamic and completely changes from your actions.

Also update added two player mode with 6 levels, they are completely different from Single player.
There is a new "mode" where Player 1 cannot eat pink boxes and Player 2 cannot eat yellow boxes.
Also in some levels Winning conditions differ - for some levels both players have to stay alive, in others only one.

##Code
Game uses lot's of code given in the Udacity HTML5 game development class. You can see the components I used in the core folder. Also Box2d used for physics and drawing in the canvas (a bit of the hack). You don't have to edit game files to add levels, they are loaded from json files. And there could be lots of interesting levels/skills/upgrades/modes added to this game. It's really extensible.

You can play it here -> http://versockas.github.io/Boxy/


##License
Sounds, images and my code is licensed under Creative Commons Attribution-ShareAlike 3.0 Unported License.

